/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 *
 * @author EstebanRM
 */
public interface RMIinterface extends Remote{
    //public int sumar(int n1, int n2) throws RemoteException; 
    public boolean validarCedula(String cedula) throws RemoteException; 
    public boolean validarRUC(String cedula) throws RemoteException; 
    public String encontrarCedula(String cedula) throws RemoteException;
}
